package com.camilo.baquero.ecommerce.listcategories

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.camilo.baquero.ecommerce.R
import com.camilo.baquero.ecommerce.ui.CategoryAdapter
import com.camilo.baquero.ecommerce.ui.CategorySelectedListener
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_list.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class CategoriesFragment : Fragment(), CategorySelectedListener {

    private val vm: CategoriesViewModel by viewModel()
    private lateinit var viewToolbar: Toolbar

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        viewToolbar = view.findViewById(R.id.toolbar)
        viewToolbar.title = "Base categories"
        viewToolbar.setNavigationOnClickListener {
            vm.goBackToParent()
        }

        vm.getTitle().observe(this, Observer { title ->
            viewToolbar.title = title
        })

        vm.getLeafCategoryIdLiveData().observe(this, Observer { categoryId ->
            //TODO the category id could also be passed through a shared ViewModel
            if (categoryId.isNotEmpty()) {
                val action = CategoriesFragmentDirections.actionCategoriesFragmentToProductsFragment(categoryId)
                nav_host_fragment.findNavController().navigate(action)
            }
        })

        categories_recycler.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = CategoryAdapter(vm,
                this@CategoriesFragment,
                this@CategoriesFragment)
        }

    }

    override fun onCategorySelected(category: Category) {
        toolbar.title = category.displayName
        vm.selectChildren(category)
    }

}
