package com.camilo.baquero.ecommerce.listproducts

import com.camilo.baquero.ecommerce.networking.ApiService

class ProductRepository(private val api: ApiService)  {
    suspend fun getProducts(categoryId: String): ProductsList = api.getCategoryProducts(categoryId)
}
