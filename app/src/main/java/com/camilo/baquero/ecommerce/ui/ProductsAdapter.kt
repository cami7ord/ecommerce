package com.camilo.baquero.ecommerce.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.camilo.baquero.ecommerce.R
import com.camilo.baquero.ecommerce.listproducts.Product
import com.camilo.baquero.ecommerce.listproducts.ProductsViewModel

class ProductsAdapter(
    viewModel: ProductsViewModel,
    lifeCycleOwner: LifecycleOwner
) : RecyclerView.Adapter<ProductsAdapter.ProductViewHolder>() {

    private var data: List<Product> = emptyList()

    init {
        viewModel.productsLiveData.observe(lifeCycleOwner, Observer { productsList ->
            data = productsList.productResults
            notifyDataSetChanged()
        })
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ProductViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int = data.size

    inner class ProductViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.list_item_category, parent, false)) {
        private var mTitleView: TextView? = null
        private var mCountView: TextView? = null

        init {
            mTitleView = itemView.findViewById(R.id.category_name)
            mCountView = itemView.findViewById(R.id.category_count)
        }

        fun bind(product: Product) {
            mTitleView?.text = product.nameShort
            mCountView?.text = "${product.productPrice.price} ${product.productPrice.currency}"
        }
    }

}
