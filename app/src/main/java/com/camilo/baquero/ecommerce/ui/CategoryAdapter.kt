package com.camilo.baquero.ecommerce.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.camilo.baquero.ecommerce.R
import com.camilo.baquero.ecommerce.listcategories.CategoriesViewModel
import com.camilo.baquero.ecommerce.listcategories.Category

class CategoryAdapter(
    viewModel: CategoriesViewModel,
    lifeCycleOwner: LifecycleOwner,
    private val listener: CategorySelectedListener
) : RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {

    private var data: List<Category> = emptyList()

    init {
        viewModel.getCategories().observe(lifeCycleOwner, Observer { baseCategory ->
            if (baseCategory.children != null) { //TODO improve with DiffUtil
                data = baseCategory.children
                notifyDataSetChanged()
            }
        })
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return CategoryViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int = data.size

    inner class CategoryViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.list_item_category, parent, false)) {
        private var mTitleView: TextView? = null
        private var mCountView: TextView? = null

        init {
            mTitleView = itemView.findViewById(R.id.category_name)
            mCountView = itemView.findViewById(R.id.category_count)
        }

        fun bind(category: Category) {
            mTitleView?.text = category.displayName
            mCountView?.text = category.children?.size.toString()
            itemView.setOnClickListener {
                listener.onCategorySelected(category)
            }
        }
    }

}
