package com.camilo.baquero.ecommerce.listcategories

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import java.util.*
import androidx.lifecycle.LiveData


class CategoriesViewModel(private val repo : CategoryRepository) : ViewModel() {

    private val categoriesLiveData = MutableLiveData<Category>()
    private val screenTitleLiveData = MutableLiveData<String>()
    private val leafCategoryIdLiveData = MutableLiveData<String>()
    private val categoryStack = Stack<Category>() //TODO This implementation is intended to just to simplify the project.

    init {
        viewModelScope.launch {
            categoriesLiveData.postValue(repo.getCategories())
        }
    }

    fun getCategories(): LiveData<Category> {
        return categoriesLiveData
    }

    fun getTitle(): LiveData<String> {
        return screenTitleLiveData
    }

    fun getLeafCategoryIdLiveData(): LiveData<String> {
        return leafCategoryIdLiveData
    }

    fun selectChildren(children: Category) {

        if (children.children.isNullOrEmpty()) {
            leafCategoryIdLiveData.value = String.format ("%.0f",  children.categoryId)
            return
        }

        screenTitleLiveData.value = children.displayName
        categoryStack.push(categoriesLiveData.value)
        categoriesLiveData.value = children
    }

    fun goBackToParent() {
        if (!categoryStack.empty()) {
            screenTitleLiveData.value = categoryStack.peek().displayName ?: "Base categories"
            categoriesLiveData.value = categoryStack.pop()
        }
    }
}
