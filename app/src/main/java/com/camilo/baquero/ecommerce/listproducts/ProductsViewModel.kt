package com.camilo.baquero.ecommerce.listproducts

import androidx.lifecycle.*
import kotlinx.coroutines.Dispatchers

class ProductsViewModel(private val repo : ProductRepository) : ViewModel() {

    private val categoryIdLiveData = MutableLiveData<String>()

    val productsLiveData = categoryIdLiveData.switchMap {
        liveData(Dispatchers.Default) { emit(repo.getProducts(it)) }
    }

    fun setCategoryId(categoryId: String) {
        categoryIdLiveData.value = categoryId
    }

}
