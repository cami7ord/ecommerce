package com.camilo.baquero.ecommerce.listproducts

data class ProductsList(
    val productResults: List<Product>
)
