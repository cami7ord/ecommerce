package com.camilo.baquero.ecommerce.networking

import com.camilo.baquero.ecommerce.listcategories.Category
import com.camilo.baquero.ecommerce.listproducts.Product
import com.camilo.baquero.ecommerce.listproducts.ProductsList
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path

interface ApiService {
    @Headers( // TODO make this an Interceptor
        "Accept: application/json",
        "Content-type: application/json",
        "appDevice: IPAD",
        "locale: de_DE"
    )
    @GET("1/category/tree")
    suspend fun getCategories(): Category

    @Headers(
        "Accept: application/json",
        "Content-type: application/json",
        "appDevice: IPAD",
        "locale: de_DE"
    )
    @GET("1/c/**/*-{categoryId}")
    suspend fun getCategoryProducts(
        @Path(value = "categoryId") categoryId: String
    ): ProductsList
}