package com.camilo.baquero.ecommerce.di

import com.camilo.baquero.ecommerce.listcategories.CategoriesViewModel
import com.camilo.baquero.ecommerce.listproducts.ProductsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    viewModel { CategoriesViewModel(get()) }
    viewModel { ProductsViewModel(get()) }
}
