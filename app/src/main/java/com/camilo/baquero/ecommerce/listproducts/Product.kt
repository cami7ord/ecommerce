package com.camilo.baquero.ecommerce.listproducts

data class Product (
    val sku: String,
    val nameShort: String,
    val productPrice: ProductPrice,
    val imageUris: List<String>
)
