package com.camilo.baquero.ecommerce.listcategories

data class Category (
    val categoryId: Double?,
    val displayName: String?,
    val resultCount: Int?,
    val children: List<Category>?
)
