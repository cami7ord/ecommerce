package com.camilo.baquero.ecommerce

import android.app.Application
import com.camilo.baquero.ecommerce.di.appModule
import com.camilo.baquero.ecommerce.di.netModule
import com.camilo.baquero.ecommerce.di.repoModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class EcommerceApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin{
            androidLogger()
            androidContext(this@EcommerceApplication)
            modules(listOf(appModule, netModule, repoModule))
        }
    }
}
