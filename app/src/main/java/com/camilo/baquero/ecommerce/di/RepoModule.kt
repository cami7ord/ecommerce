package com.camilo.baquero.ecommerce.di

import com.camilo.baquero.ecommerce.listcategories.CategoryRepository
import com.camilo.baquero.ecommerce.listproducts.ProductRepository
import org.koin.dsl.module

val repoModule = module {
    factory { CategoryRepository(get()) }
    factory { ProductRepository(get()) }
}
