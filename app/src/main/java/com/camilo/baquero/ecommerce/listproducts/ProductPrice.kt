package com.camilo.baquero.ecommerce.listproducts

data class ProductPrice (
    val price: Double,
    val currency: String
)
