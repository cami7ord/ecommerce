package com.camilo.baquero.ecommerce.listproducts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.camilo.baquero.ecommerce.R
import com.camilo.baquero.ecommerce.ui.ProductsAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_list.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProductsFragment : Fragment() {

    private val vm: ProductsViewModel by viewModel()
    private val args: ProductsFragmentArgs by navArgs()
    private lateinit var viewToolbar: Toolbar

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        vm.setCategoryId(args.categoryId)

        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewToolbar = view.findViewById(R.id.toolbar)
        viewToolbar.setNavigationOnClickListener {
            nav_host_fragment.findNavController().navigate(R.id.categoriesFragment)//TODO The navigation needs more love <3
        }

        requireActivity().onBackPressedDispatcher.addCallback(this) {
            nav_host_fragment.findNavController().navigate(R.id.categoriesFragment)
        }
        viewToolbar.title = "Products"

        categories_recycler.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = ProductsAdapter(vm, this@ProductsFragment)
        }

    }

}
