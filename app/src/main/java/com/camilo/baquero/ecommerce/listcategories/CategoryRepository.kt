package com.camilo.baquero.ecommerce.listcategories

import com.camilo.baquero.ecommerce.networking.ApiService

class CategoryRepository(private val api: ApiService) {
    suspend fun getCategories(): Category = api.getCategories()
}
