package com.camilo.baquero.ecommerce.ui

import com.camilo.baquero.ecommerce.listcategories.Category

interface CategorySelectedListener {

    fun onCategorySelected(category: Category)

}
